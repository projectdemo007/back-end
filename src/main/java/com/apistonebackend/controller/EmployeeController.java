package com.apistonebackend.controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.apistonebackend.model.Employee;
import com.apistonebackend.service.EmployeeService;

@RestController
@CrossOrigin(origins="http://localhost:3000")
public class EmployeeController 
{
	@Autowired
	private EmployeeService employeeService;

	//To get data of existing employee
	@GetMapping("/getEmployee/{id}")
	public ResponseEntity<Object> getEmployeeById(@PathVariable String id)
	{
		Employee employee;
		try {
			employee = employeeService.getEmployeeById(id).get();
			Map<String,String> linkedHashMap = new LinkedHashMap<String, String>();
			linkedHashMap.put("Emp ID",employee.getEmpId());
			linkedHashMap.put("Emp Name",employee.getEmpName());
			for(int i = 0;i<employee.getAdditionalField().size();i++)
			{
				Set<String> keys = employee.getAdditionalField().get(i).keySet();
				for(String str : keys)
				{
					linkedHashMap.put(str, employee.getAdditionalField().get(i).get(str));
				}
			}
			return new ResponseEntity<Object>(linkedHashMap,HttpStatus.OK);
		}
		catch(NoSuchElementException exp)
		{
			employee = null;
		}
		
		return new ResponseEntity<Object>("[]",HttpStatus.OK);
	}
	
	//To insert new or update existing employee
	@PostMapping("/addEmployee")
	public ResponseEntity<Object> addEmployee(@RequestBody Employee employee)
	{	
		System.out.println(employee);
		employeeService.addEmployee(employee);
		return new ResponseEntity<Object>("{message : Employee added}",HttpStatus.CREATED);
	}
	
}
