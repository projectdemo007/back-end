package com.apistonebackend.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Simple POJO  representing an employee
 */

@Document(collection="Employee")
public class Employee
{
	//Keeping empId and empName as fixed fields
	@Id
	private String empId;
	private String empName;
	private List<Map<String,String>> additionalField; //All remaining fields will be in additional field
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public List<Map<String, String>> getAdditionalField() {
		return additionalField;
	}
	public void setAdditionalField(List<Map<String, String>> additionalField) {
		this.additionalField = additionalField;
	}
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", additionalField="
				+ additionalField + "]";
	}
	
	
	
	
	
	
}
