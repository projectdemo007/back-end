package com.apistonebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApistoneBackend {

	public static void main(String[] args) {
		SpringApplication.run(ApistoneBackend.class, args);
	}

}
