package com.apistonebackend.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apistonebackend.model.Employee;
import com.apistonebackend.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	//To get data of existing employee
	public Optional<Employee> getEmployeeById(String id)
	{
		return employeeRepository.findById(id);
	}
	
	//To insert new or update existing employee
	public void addEmployee(Employee employee)
	{	
		employeeRepository.save(employee);
	}

}
